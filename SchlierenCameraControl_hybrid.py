# -*- coding: utf-8 -*-
"""
Created on Sun Nov  1 20:18:53 2020

@author: Christoph
"""


#!/usr/bin/python
# import tkinter as tk
from tkinter import *
from tkinter import ttk
from tkinter import filedialog 
from tkinter import Tk, Label, Button, Frame, Entry, OptionMenu, messagebox, Scale
#import RPi.GPIO as GPIO
import picamera
from time import sleep
import sys
import subprocess

global camera
camera = picamera.PiCamera(resolution = (3280, 2464))
camera.iso = 100
sleep(2)
camera.exposure_mode = 'off'

camera.awb_mode = 'off'
camera.awb_gains = (0.9,0.9)
# camera.digital_gain = 1.0
# camera.analog_gain = 1.0

#camera.rotation = 180#sensor_mode = 0)
choices = ['off', 'auto', 'sunlight','cloudy','shade','tungsten','fluorescent','incandescent','flash','horizon']
isos = [100, 200, 400, 800]
effects = ['none','negative','solarize','sketch','denoise','emboss','oilpaint','hatch','gpen','pastel','watercolor','film','blur','saturation','colorswap','washedout','posterise','colorpoint','colorbalance','cartoon','deinterlace1','deinterlace2']

class Microscope_GUI:
    def __init__(self, master):
        self.master = master
        global image_name
        # self.image_name = StringVar()
        global camera
        self.path = '/home/pi/Pictures'
        self.AWB_Var = StringVar()
        self.AWB_Var.set(choices[0])
        self.EFFECT_Var = StringVar()
        self.EFFECT_Var.set(effects[0])
        self.iso_value = StringVar()
        self.iso_value.set(isos[0])
        self.exposure = StringVar(value = 5000)
        self.delay = 1.0
        self.image_name = StringVar()
        self.create_widgets()

        
    
    def create_widgets(self):
        self.content = Frame(self.master)
        self.frame = Frame(self.content, relief="ridge", width=640, height=480)
        self.buttonframe = Frame(self.content, width = 800-640, height=480)
        self.content.grid(column=0, row=0, columnspan=4)
        self.frame.grid(column=0, row=0, columnspan=3, rowspan=5)
        self.buttonframe.grid(row=0, column=3, rowspan=5, sticky='nesw')
        
        #all buttons and entries
        self.startbutton = Button(self.buttonframe, text='Start Preview', font=('Helvetica', '12'), command=self.CameraON)
        self.startbutton.grid(row=3, column = 0,pady=4, sticky='nesw')
        self.camera_off = Button(self.buttonframe, text='Stop Preview', command=self.CameraOFF)
        self.camera_off.grid(row=4, column = 0, pady=4, sticky='nesw')
        self.save_to = Button(self.buttonframe, text='Save to', font=('Helvetica', '12'), command=self.Select_Path)
        self.save_to.grid(row=2, column = 0,pady=2, sticky='nesw')
        #image name and capture
        self.namelabel = Label(self.buttonframe, text='Image name', font=('Helvetica', '12')).grid(row=0, column = 0, pady=(1,1), sticky='nesw')
        self.nameentry = Entry(self.buttonframe, textvariable=self.image_name, font=('Helvetica', '12'), width = 12)
        self.nameentry.grid(row=1, column = 0, pady=2, sticky='nesw')
        self.capture = Button(self.buttonframe, text='Capture', command=self.Capture, font=('Helvetica', '17'), bg = 'green')
        self.capture.grid(row=5, column = 0, pady=(4,5), sticky='nesw')
        # white balance and effects
        self.AWBlabel = Label(self.buttonframe, text='AWB options', font=('Helvetica', '12')).grid(row=6, column = 0, pady=(10,0), sticky='nesw')
        self.AWB_Option = OptionMenu(self.buttonframe, self.AWB_Var, *choices, command=SetAWB).grid(row=7, column = 0, pady=(0,4), sticky='nesw')
        # effect options
#         self.Efflabel = Label(self.buttonframe, text='Effect options', font=('Helvetica', '12')).grid(row=8, column = 0, pady=(10,0), sticky='nesw')
#         self.Eff_Option = OptionMenu(self.buttonframe, self.EFFECT_Var, *effects, command=SetEFFECTS).grid(row=9, column = 0, pady=(0,4), sticky='nesw')
        # iso options
        self.ISOlabel = Label(self.buttonframe, text='ISO options', font=('Helvetica', '12')).grid(row=8, column = 0, pady=(10,0), sticky='nesw')
        self.ISO_Option = OptionMenu(self.buttonframe, self.iso_value, *isos, command=SetISO)
        self.ISO_Option.grid(row=9, column = 0, pady=(0,4), sticky='nesw')
        self.exposurelabel = Label(self.buttonframe, text='Exposure in µs', font=('Helvetica', '12')).grid(row=10, column = 0, pady=(1,1), sticky='nesw')
        self.exposureentry = Entry(self.buttonframe, textvariable=self.exposure, font=('Helvetica', '12'), width = 12)
        self.exposureentry.bind('<Return>', self.SetExposure)
        self.exposureentry.grid(row=11, column = 0, pady=2, sticky='nesw')
        self.Delay_Option = Scale(self.buttonframe, from_=1, to=20, orient=HORIZONTAL, label = "Delay in s", command=self.UpdateDelay)
        self.Delay_Option.grid(row=12, column = 0, pady=(1,3), sticky='nesw')
        
        self.exitframe = Button(self.buttonframe, text='Exit', command=EXIT).grid(row=13, column = 0, pady=(8,4), sticky='nesw')
        
        
    def Select_Path(self):
        newpath = filedialog.askdirectory(initialdir=self.path, title='Save image to...')
        self.path = newpath
        print(self.path)
        return self.path
    
    def Capture(self): #Image Acquisition
        global camera
        if self.image_name == '' or self.path == '/mnt/smb':
            self.CameraOFF()
            messagebox.showerror("Error", "The image has no name or valid save directory. Please make sure to give it one and to select the path in which the image should be stored. The preview will now be stopped to allow you to do so. After giving the image a name and select the path, you can start the preview and the capture.")
        else:
            camera.stop_preview()
            camera.close()
            
#             camera.resolution = (3280, 2464)
#             camera.preview_window=(root.winfo_x(), root.winfo_y(), 640, 480)
#             camera.start_preview(resolution=(640,480))
#             camera.iso = 100
#             sleep(2)
# 
#             camera.shutter_speed = int(self.exposure.get())
#             print('Shutter speed is '+str(int(self.exposure.get())))
#             camera.exposure_mode = 'off'
#             camera.awb_mode = 'off'
#             camera.awb_gains = (1.0,1.0)

            #camera.resolution = (3840, 2160) #4K
             #4:3
#             camera.resolution = (2592, 1944)
            #camera.resolution = (4056, 3040)
            #camera.sensor_mode = 2
            sleep(float(self.delay))
            #imagename = self.nameentry.get('1.0', END)
            print('textvariable is '+self.nameentry.get())
            imagefullpath = self.path+'/'+self.nameentry.get()
            
            subprocess.check_call(['raspistill',
                                   '-md','3',
                                   '-ISO',str(int(self.iso_value.get())),
                                   '-ex','auto',                                   
                                   '-ss',str(int(self.exposure.get())),
                                   '-awb','off',
                                   '-awbg','1.0,1.0',
                                   '-ag','1.0',
                                   '-dg','1.0',                                   
                                   '-q','100',
                                   '-o',imagefullpath+'.jpg'
                                   ])
#             camera.capture(imagefullpath)
            sleep(0.1)
            subprocess.check_call(['raspiyuv',
                       '-md','3',
                       '-ISO',str(int(self.iso_value.get())),
                       '-ex','auto',                                   
                       '-ss',str(int(self.exposure.get())),
                       '-awb','off',
                       '-awbg','1.0,1.0',
                       '-ag','1.0',
                       '-dg','1.0',                                   
                       '-o',imagefullpath+'.data'
                       ])
#             imagefullpath = self.path+'/'+self.nameentry.get()+'.png'
#             camera.capture(imagefullpath, 'png')
            sleep(0.3)
            camera = picamera.PiCamera(resolution = (640,480))
            camera.iso = 100
            sleep(2)
            camera.exposure_mode = 'off'

            camera.awb_mode = 'off'
            camera.awb_gains = (0.9,0.9)
            self.CameraON()

    def CameraON(self):
        global camera
        camera.preview_fullscreen=False
        #camera.preview_layer = 0
        camera.resolution=(640,480)
        camera.preview_window=(root.winfo_x(), root.winfo_y(), 640, 480)
        camera.start_preview(resolution=(640,480))
        self.save_to['state'] = DISABLED
        #self.save_to.config(state=DISABLED)
    
    def CameraOFF(self):
        global camera
        camera.stop_preview()
        self.save_to['state'] = NORMAL
 
    def UpdateDelay(self, value):
        self.delay = value
        
    def SetExposure(self, value):
        global camera
        camera.shutter_speed = int(self.exposure.get())
        print('value is '+str(value))
        print('Changed exposure to'+self.exposure.get())


    
def EXIT():
    root.destroy()
    # camera.stop_preview()
    # camera.close()
    sys.exit()
    
    


def UpdateBrightness(value):
    camera.brightness = int(value)
    
def UpdateContrast(value):
    camera.contrast = int(value)
    
def UpdateSharpness(value):
    camera.sharpness = int(value)
    
def UpdateSaturation(value):
    camera.saturation = int(value)

def SetAWB(var):
    camera.awb_mode = var

def SetEFFECTS(var):
    camera.image_effect = var

def SetISO(var):
    global camera
    camera.exposure_mode = 'auto'
#     camera.awb_mode = 'auto'
    print('ISO var is '+str(var))
#     camera.iso = int(self.iso_value.get())
    camera.iso = int(var)
    sleep(0.5)
    camera.exposure_mode = 'off'


def Zoom(var):
    x = float("0."+var)
    camera.zoom = (0.5,0.5,x,x)

if __name__ == "__main__":  
    root = Tk()
    root.resizable(width=False, height=False)
    #root.wm_attributes('-fullscreen','true')
    root.geometry("800x480+0+0")
    root.title("CG Microscope Tkinter Test")
    my_gui = Microscope_GUI(root)
    root.mainloop()
    